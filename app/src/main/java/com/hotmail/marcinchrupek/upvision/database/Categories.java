package com.hotmail.marcinchrupek.upvision.database;

import com.hotmail.marcinchrupek.upvision.requests.POJOs.Category;
import com.hotmail.marcinchrupek.upvision.requests.POJOs.Row;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "categories")
public class Categories {
    @DatabaseField(generatedId = true, unique = true)
    private int id;
    @DatabaseField(unique = true)
    private String name;
    @DatabaseField
    private String imageURL;
    @DatabaseField
    private String smallImageURL;

    public Categories() {
    }

    public Categories(Row row) {
        this.name = row.name;
        if(row.image != null) {
            this.imageURL = row.image.filePath;
            this.smallImageURL = row.image.filePathSmall;
        } else {
            this.imageURL = "";
            this.smallImageURL = "";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public void setSmallImageURL(String smallImageURL) {
        this.smallImageURL = smallImageURL;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", smallImageURL='" + smallImageURL + '\'' +
                '}';
    }
}
