package com.hotmail.marcinchrupek.upvision.dagger;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hotmail.marcinchrupek.upvision.database.DatabaseHelper;
import com.hotmail.marcinchrupek.upvision.requests.InterfaceAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {
    private final SetupApplication application;

    public ApiModule(SetupApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofitBuilder(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(InterfaceAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    @Provides
    @Singleton
    InterfaceAPI provideApiInterface(Retrofit builder) {
        return builder.create(InterfaceAPI.class);
    }

    @Provides
    @Singleton
//    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    DatabaseHelper provideDatabaseHelper() {
        return new DatabaseHelper(application);
    }


}
