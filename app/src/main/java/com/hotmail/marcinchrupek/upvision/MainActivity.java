package com.hotmail.marcinchrupek.upvision;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.hotmail.marcinchrupek.upvision.activities.ListActivity;
import com.hotmail.marcinchrupek.upvision.dagger.SetupApplication;
import com.hotmail.marcinchrupek.upvision.database.DatabaseHelper;
import com.hotmail.marcinchrupek.upvision.requests.InterfaceAPI;
import com.hotmail.marcinchrupek.upvision.requests.RequestHelper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static int TIME_OUT = 3000;

    @BindView(R.id.button)
    Button button;
    @Inject
    InterfaceAPI interfaceAPI;
    @Inject
    DatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        SetupApplication.get(this).getAppComponent().inject(this);

        tryToConnect();

    }

    @OnClick(R.id.button)
    public void buttonClicked(){
        tryToConnect();
    }

    private void tryToConnect(){
        if(isNetworkAvailable()) {
            RequestHelper.launchRequest(interfaceAPI, getApplicationContext(), helper);

            button.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(getBaseContext(), ListActivity.class);
                    startActivity(i);

                    finish();
                }
            }, TIME_OUT);

        } else {
            button.setVisibility(View.VISIBLE);
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("Turn on the internet!");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
