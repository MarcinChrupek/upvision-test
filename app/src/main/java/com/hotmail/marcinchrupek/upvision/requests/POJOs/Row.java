package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Row {

    @SerializedName("venue")
    @Expose
    public Venue venue;
    @SerializedName("lastEditDate")
    @Expose
    public LastEditDate lastEditDate;
    @SerializedName("image")
    @Expose
    public Image image;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("uniqueId")
    @Expose
    public String uniqueId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("deleted")
    @Expose
    public Boolean deleted;

    @Override
    public String toString() {
        return "Row{" +
                "venue=" + venue +
                ", lastEditDate=" + lastEditDate +
                ", image=" + image +
                ", id=" + id +
                ", uniqueId='" + uniqueId + '\'' +
                ", name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", color='" + color + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}
