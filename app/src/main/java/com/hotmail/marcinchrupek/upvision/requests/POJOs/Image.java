package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("uniqueId")
    @Expose
    public String uniqueId;
    @SerializedName("filePath")
    @Expose
    public String filePath;
    @SerializedName("createDate")
    @Expose
    public CreateDate createDate;
    @SerializedName("defaultImage")
    @Expose
    public Boolean defaultImage;
    @SerializedName("filePathSmall")
    @Expose
    public String filePathSmall;

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", uniqueId='" + uniqueId + '\'' +
                ", filePath='" + filePath + '\'' +
                ", createDate=" + createDate +
                ", defaultImage=" + defaultImage +
                ", filePathSmall='" + filePathSmall + '\'' +
                '}';
    }
}
