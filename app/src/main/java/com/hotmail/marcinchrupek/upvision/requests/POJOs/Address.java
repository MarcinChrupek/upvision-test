package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("street")
    @Expose
    public String street;
    @SerializedName("buildNr")
    @Expose
    public String buildNr;
    @SerializedName("flatNr")
    @Expose
    public String flatNr;
    @SerializedName("zipCode")
    @Expose
    public String zipCode;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("country")
    @Expose
    public String country;

}
