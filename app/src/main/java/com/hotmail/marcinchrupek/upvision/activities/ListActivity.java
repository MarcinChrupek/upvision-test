package com.hotmail.marcinchrupek.upvision.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;

import com.hotmail.marcinchrupek.upvision.R;
import com.hotmail.marcinchrupek.upvision.dagger.SetupApplication;
import com.hotmail.marcinchrupek.upvision.database.Categories;
import com.hotmail.marcinchrupek.upvision.database.DatabaseHelper;
import com.hotmail.marcinchrupek.upvision.utils.RecyclerViewAdapter;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    DatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        SetupApplication.get(this).getAppComponent().inject(this);

        toolbar.setTitle("List");
        setSupportActionBar(toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        List<Categories> categoriesList;
        try {
            categoriesList = helper.getCategoriesList();
            RecyclerViewAdapter adapter = new RecyclerViewAdapter(categoriesList, getApplicationContext());
            adapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    //TODO:przejscie do activity kategorii
                }
            });
            recyclerView.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
