package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Category {

    @SerializedName("total")
    @Expose
    public Integer total;

    @SerializedName("rows")
    @Expose
    public List<Row> rows = new ArrayList<Row>();

    @Override
    public String toString() {
        return "Category{" +
                "total=" + total +
                ", rows=" + rows +
                '}';
    }
}