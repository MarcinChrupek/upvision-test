package com.hotmail.marcinchrupek.upvision.dagger;

import android.app.Application;
import android.content.Context;

import com.hotmail.marcinchrupek.upvision.MainActivity;
import com.hotmail.marcinchrupek.upvision.activities.ListActivity;

import javax.inject.Singleton;

import dagger.Component;

public class SetupApplication extends Application {

    @Singleton
    @Component(modules = ApiModule.class)
    public interface AppComponent {
        void inject(MainActivity mainActivity);
        void inject(ListActivity listActivity);
    }

    private AppComponent appComponent;

    public static SetupApplication get(Context context) {
        return (SetupApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerSetupApplication_AppComponent.builder()
                .apiModule(new ApiModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}