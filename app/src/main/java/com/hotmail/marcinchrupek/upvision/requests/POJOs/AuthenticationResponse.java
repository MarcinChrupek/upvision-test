package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcin on 2016-07-04.
 */
public class AuthenticationResponse {

    @SerializedName("access_token")
    @Expose
    public String accessToken;

    @SerializedName("token_type")
    @Expose
    public String tokenType;

    @SerializedName("refresh_token")
    @Expose
    public String refreshToken;

    @SerializedName("expires_in")
    @Expose
    public int expiresIn;

    @SerializedName("scope")
    @Expose
    public String scope;

    @Override
    public String toString() {
        return
                "accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expiresIn=" + expiresIn +
                ", scope='" + scope + '\'' ;
    }

}
