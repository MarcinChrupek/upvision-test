package com.hotmail.marcinchrupek.upvision.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hotmail.marcinchrupek.upvision.R;
import com.hotmail.marcinchrupek.upvision.database.Categories;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Categories> dataset;
    private Context context;
    private OnItemClickListener onItemClickListener;


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public  void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        @BindView(R.id.title)
        public TextView title;

        @BindView(R.id.imageView)
        public ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public RecyclerViewAdapter(List<Categories> dataset, Context context) {
        this.dataset = dataset;
        this.context = context;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_element, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(dataset.get(position).getName());
        if(!dataset.get(position).getImageURL().equals("")){
            String url ="http://demo.gopos.pl/" + dataset.get(position).getImageURL();
            Picasso.with(context).load(url).into(holder.imageView);
        } else {
            Picasso.with(context).load(android.R.drawable.sym_def_app_icon).into(holder.imageView);
        }
    }


    @Override
    public int getItemCount() {
        return dataset.size();
    }

}