package com.hotmail.marcinchrupek.upvision.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hotmail.marcinchrupek.upvision.requests.POJOs.Category;
import com.hotmail.marcinchrupek.upvision.requests.POJOs.Row;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "orm1.db";
    private static final int DATABASE_VERSION = 1;
    private Dao<Categories, Integer> categoriesDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Categories.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Categories.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        super.close();
        categoriesDao = null;
    }

    public Dao<Categories, Integer> getCategoriesDao() throws SQLException {
        if (categoriesDao == null) {
            categoriesDao = getDao(Categories.class);
        }
        return categoriesDao;
    }

    public List<Categories> getCategoriesList()throws SQLException {
        if (categoriesDao == null) {
            categoriesDao = getDao(Categories.class);
        }
        return categoriesDao.queryForAll();
    }

    public void addCategories(Category category) {
        for (Row row : category.rows) {
            Categories categories = new Categories(row);
            try {
                getCategoriesDao().createOrUpdate(categories);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
