package com.hotmail.marcinchrupek.upvision.requests;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.hotmail.marcinchrupek.upvision.R;
import com.hotmail.marcinchrupek.upvision.database.DatabaseHelper;
import com.hotmail.marcinchrupek.upvision.requests.POJOs.AuthenticationResponse;
import com.hotmail.marcinchrupek.upvision.requests.POJOs.Category;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestHelper {
    private final static String USERNAME = "test@gmail.com";
    private final static String PASSWORD = "test2016";
    private final static String CLIENT_ID = "06dff5d1-07b1-40f9-80bd-625db272f8da";
    private final static String CLIENT_SECRET = "b5847be0-caf5-483f-a234-757977fa7f37";
    private final static String GRANT_TYPE = "password";

    public static void launchRequest(final InterfaceAPI service, final Context context, final DatabaseHelper helper) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String token = prefs.getString(context.getResources().getString(R.string.access_token), "");

        if(token.equals("")){
            getAccessToken(service, context, helper);
        } else {
            refreshToken(service, context, helper);
        }

    }


    public static void getAccessToken(final InterfaceAPI service, final Context context, final DatabaseHelper helper) {
        Call<AuthenticationResponse> call = service.getAccessToken(USERNAME, PASSWORD, CLIENT_ID, CLIENT_SECRET, GRANT_TYPE);

        call.enqueue(new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                if (response.isSuccessful()) {
                    saveInPrefs(response.body(), context);

                    getCategories(service, context, helper);
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                Log.d("RequestHelper", "onFailure " + t.getLocalizedMessage());
            }
        });
    }

    public static void getCategories(InterfaceAPI service, final Context context, final DatabaseHelper helper) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String token = prefs.getString(context.getResources().getString(R.string.access_token), "");

        Call<Category> call = service.getCategories(token);

        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                if (response.isSuccessful()) {
                    helper.addCategories(response.body());
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                Log.d("RequestHelper", "onFailure " + t.getLocalizedMessage());
            }
        });
    }

    public static void refreshToken(final InterfaceAPI service, final Context context, final DatabaseHelper helper) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String token = prefs.getString(context.getResources().getString(R.string.refresh_token), "");

        Call<AuthenticationResponse> call = service.getRefreshToken(CLIENT_ID, CLIENT_SECRET, "refresh_token", token);

        call.enqueue(new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                if (response.isSuccessful()) {
                    saveInPrefs(response.body(), context);

                    getCategories(service, context, helper);
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                Log.d("RequestHelper", "onFailure " + t.getLocalizedMessage());
            }
        });
    }


    private static void saveInPrefs(AuthenticationResponse response, Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(context.getResources().getString(R.string.access_token), response.accessToken);
        editor.putString(context.getResources().getString(R.string.refresh_token), response.refreshToken);
        editor.apply();
    }

}
