package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chronology {

    @SerializedName("calendarType")
    @Expose
    public String calendarType;
    @SerializedName("id")
    @Expose
    public String id;

}