package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Venue {

    @SerializedName("uniqueId")
    @Expose
    public String uniqueId;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("alias")
    @Expose
    public String alias;
    @SerializedName("address")
    @Expose
    public Address address;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("defaultOpenHours")
    @Expose
    public Integer defaultOpenHours;

    @Override
    public String toString() {
        return "Venue{" +
                "uniqueId='" + uniqueId + '\'' +
                ", id=" + id +
                ", alias='" + alias + '\'' +
                ", address=" + address +
                ", name='" + name + '\'' +
                ", defaultOpenHours=" + defaultOpenHours +
                '}';
    }
}