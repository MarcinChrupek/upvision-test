package com.hotmail.marcinchrupek.upvision.requests;

import com.hotmail.marcinchrupek.upvision.requests.POJOs.AuthenticationResponse;
import com.hotmail.marcinchrupek.upvision.requests.POJOs.Category;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InterfaceAPI {
    String BASE_URL = "http://demo.gopos.pl/";
    String TOKEN_URL = "oauth/token";
    String CATEGORY_URL = "api/sale/venue/6/productCategory";

    @GET(TOKEN_URL)
    Call<AuthenticationResponse> getAccessToken(@Query("username") String username, @Query("password") String password,
                              @Query("client_id") String client_id, @Query("client_secret") String client_secret,
                              @Query("grant_type") String grant_type);

    @GET(CATEGORY_URL)
    Call<Category> getCategories(@Query("access_token") String token);

    @GET(TOKEN_URL)
    Call<AuthenticationResponse> getRefreshToken(@Query("client_id") String client_id, @Query("client_secret") String client_secret,
                                @Query("grant_type") String grant_type, @Query("refresh_token") String refresh_token);
}
