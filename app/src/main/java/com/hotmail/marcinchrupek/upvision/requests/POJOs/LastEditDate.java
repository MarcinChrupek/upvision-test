package com.hotmail.marcinchrupek.upvision.requests.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastEditDate {

    @SerializedName("dayOfYear")
    @Expose
    public Integer dayOfYear;
    @SerializedName("year")
    @Expose
    public Integer year;
    @SerializedName("month")
    @Expose
    public String month;
    @SerializedName("monthValue")
    @Expose
    public Integer monthValue;
    @SerializedName("dayOfMonth")
    @Expose
    public Integer dayOfMonth;
    @SerializedName("hour")
    @Expose
    public Integer hour;
    @SerializedName("minute")
    @Expose
    public Integer minute;
    @SerializedName("second")
    @Expose
    public Integer second;
    @SerializedName("nano")
    @Expose
    public Integer nano;
    @SerializedName("dayOfWeek")
    @Expose
    public String dayOfWeek;
    @SerializedName("chronology")
    @Expose
    public Chronology chronology;

}